﻿using System;

using Xamarin.Forms;

namespace FirstFormsApp
{
	public class App : Application
	{
		public App()
		{
//			// The root page of your application
//			MainPage = new ContentPage {
//				Content = new StackLayout {
//					VerticalOptions = LayoutOptions.Center,
//					Children = {
//						new Label {
//							XAlign = TextAlignment.Center,
//							Text = "Welcome to Xamarin Forms!"
//						}
//					}
//				}
//			};

			var layout = new StackLayout() 
			{
				VerticalOptions = LayoutOptions.Center,
				Children = 
				{
					new Label() 
					{
						XAlign = TextAlignment.Center,
						Text = "Welcome to Xamarin Forms!"
					}
				}
			};

			MainPage = new ContentPage {
				Content = layout
			};

			Button button = new Button {
				Text = "Click me"
			};

			button.Clicked += async (sender, e) =>
			{
				await MainPage.DisplayAlert("Alert", "You clicked me", "OK");
			};

			layout.Children.Add(button);
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}

